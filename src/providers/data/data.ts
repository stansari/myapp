
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/*
  Generated class for the DataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataProvider {
lists: any = [
  {itemName: 'Shahegul', MobileNo : "123456789", city : 'Nagpur'},
  {itemName: "Sheeba Ansari", MobileNo : "54511212", city : 'Pune'},
  {itemName: "Fauziya", MobileNo : "54545212", city : 'Bangalore'},
  {itemName: "Neeba", MobileNo : "44154564", city : 'mumbai'},
  {itemName: "Bilquis", MobileNo : "013242256", city : 'agra'},
  {itemName: "Iqra", MobileNo : "5435123", city : 'lahore'},
  {itemName: "Farheen", MobileNo : "6546565", city : 'delhi'},
  {itemName: "Mus'ab", MobileNo : "51515320", city : 'Goa'},
  {itemName: "Tahir", MobileNo : "8789654561", city : 'Amravati'},
  {itemName: "Ishaque", MobileNo : "21200555", city : 'Ahmedabad'},
  {itemName: "Sneha", MobileNo : "021654665", city : 'Chandrapur'},
  {itemName: "Diksha", MobileNo : "58212551", city : 'Hingna'},
  {itemName: "Akansha", MobileNo : "6852330", city : 'Chindwada'},
  {itemName: "Imama", MobileNo : "32154200", city : 'morshi'},
  {itemName: "Humain", MobileNo : "543212315", city : 'Aurangabad'}

];
  constructor() {
    console.log('Hello DataProvider Provider');
  }
}
