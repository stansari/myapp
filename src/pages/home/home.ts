import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  homelists: any;
  descending: boolean = false;
  order: number;
  column: string = 'name';
  constructor(public navCtrl: NavController, public data:DataProvider) {

  }
ionViewDidLoad()
{
  this.homelists=this.data.lists;
}
itemClicked(item, page): void {
  this.navCtrl.push (page ='InfoPage', item);
}
sort(){
  this.descending = !this.descending;
  this.order = this.descending ? 1 : -1;
}
}
